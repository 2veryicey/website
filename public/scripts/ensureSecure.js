let host = "testground.fpdev.net";
if ((host === window.location.host) && (window.location.protocol !== 'https:')) {
    window.location = window.location.toString().replace(/^http:/, "https:");
}
