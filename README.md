**This Repository will serve as a sort of playground for me to learn the ropes of web development. It will evolve as I learn more of the ins-and-outs of this area.**

The final product can be viewed at 2veryicey.gitlab.io/website (Custom Domain coming soon, I am working on changing from an old domain name, to a new one).

Sorry if it is utterly dreadful, as I said this repository will be around around the edges and serve as a testing-ground for what I learn.

I am currently being self-taught, however if you know of good (**FREE**) resources on this please shoot me an email (It is on my profile)!

